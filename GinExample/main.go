package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Student struct {
	Name string
	Age  int8
}

func main() {
	r := gin.Default()
	r.LoadHTMLGlob("template/*")
	//gin.SetMode(gin.ReleaseMode)
	r.GET("/", func(g *gin.Context) {
		g.JSON(200, gin.H{
			"message": "gin",
		})
	})

	stu1 := &Student{Name: "TestA", Age: 15}
	stu2 := &Student{Name: "TestB", Age: 18}

	r.GET("/arr", func(g *gin.Context) {
		g.HTML(http.StatusOK, "arr.tmpl", gin.H{
			"title":   "gin1",
			"studArr": [2]*Student{stu1, stu2},
		})
	})
	r.POST("/form", func(c *gin.Context) {
		username := c.PostForm("username")
		password := c.DefaultPostForm("password", "000000") // default password

		c.JSON(http.StatusOK, gin.H{
			"username": username,
			"password": password,
		})
	})
	r.Run()
}
