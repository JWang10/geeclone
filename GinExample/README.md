# Gin simple example

## 遇到問題
  
  - m1 mac (arm64) 無法執行 Gin framework
```go
# golang.org/x/sys/unix
../../../pkg/mod/golang.org/x/sys@v0.0.0-20200116001909-b77594299b42/unix/syscall_darwin.1_13.go:25:3: //go:linkname must refer to declared function or variable
../../../pkg/mod/golang.org/x/sys@v0.0.0-20200116001909-b77594299b42/unix/zsyscall_darwin_arm64.1_13.go:27:3: //go:linkname must refer to declared function or variable
../../../pkg/mod/golang.org/x/sys@v0.0.0-20200116001909-b77594299b42/unix/zsyscall_darwin_arm64.1_13.go:40:3: //go:linkname must refer to declared function or variable
../../../pkg/mod/golang.org/x/sys@v0.0.0-20200116001909-b77594299b42/unix/zsyscall_darwin_arm64.go:28:3: //go:linkname must refer to declared function or variable
../../../pkg/mod/golang.org/x/sys@v0.0.0-20200116001909-b77594299b42/unix/zsyscall_darwin_arm64.go:43:3: //go:linkname must refer to declared function or variable

```

## 解決方案

  ```go
  go get -u golang.org/x/sys // 更新這個package即可
  ```