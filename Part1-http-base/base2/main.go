package main

import (
	"fmt"
	"log"
	"net/http"
)

// 實現`http.Handler`接口
//type Handler interface {
//	ServeHTTP(ResponseWriter, *Request)
//}
//func ListenAndServe(addr string, handler Handler) error

// Engine is only a handler for all requests
type Engine struct{}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/":
		fmt.Fprintf(w, "URL.Path= %q\n", r.URL.Path)

	case "/hello":
		for k, v := range r.Header {
			fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
		}
	default:
		fmt.Fprintf(w, "404 Not found: %s\n", r.URL)
	}
}

func main() {
	engine := new(Engine)
	log.Fatalln(http.ListenAndServe(":8080", engine))
}
