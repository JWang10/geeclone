package main

import (
	"fmt"
	"gee"
	"net/http"
)

//$ curl http://localhost:8080
//hello, gee server
//URL.Path = "/"
//$ curl http://localhost:8080/hello
//Header["User-Agent"] = ["curl/7.79.1"]
//Header["Accept"] = ["*/*"]

func main() {
	r := gee.New()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "hello, gee server\n")
		fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)
	})
	r.Get("/hello", func(w http.ResponseWriter, r *http.Request) {
		for k, v := range r.Header {
			fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
		}
	})
	r.Run(":8080")
}
