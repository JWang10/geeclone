package gee

import (
	"fmt"
	"net/http"
)

// HandlerFunc defines the request handler are uesd by gee
type HandlerFunc func(w http.ResponseWriter, r *http.Request)

// Engine implements the interface of ServeHTTP (nep/http - Handler interface)
type Engine struct {
	router map[string]HandlerFunc
}

// New a instance of Engine, is the constructor of gee.Engine
func New() *Engine {
	return &Engine{router: make(map[string]HandlerFunc)}
}

func (engine *Engine) addRoute(method, pattern string, handler HandlerFunc) {
	key := method + "-" + pattern
	engine.router[key] = handler
}

// Get method request
func (engine *Engine) Get(pattern string, handler HandlerFunc) {
	engine.addRoute("GET", pattern, handler)
}

// Post method request
func (engine *Engine) Post(pattern string, handler HandlerFunc) {
	engine.addRoute("POST", pattern, handler)
}

// Update method request
func (engine *Engine) Update(pattern string, handler HandlerFunc) {
	engine.addRoute("UPDATE", pattern, handler)
}

// Delete method request
func (engine *Engine) Delete(pattern string, handler HandlerFunc) {
	engine.addRoute("DELETE", pattern, handler)
}

// Run defines the method to start a web server,
func (engine *Engine) Run(addr string) (err error) {
	return http.ListenAndServe(addr, engine)
}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	key := r.Method + "-" + r.URL.Path
	if handler, ok := engine.router[key]; ok {
		handler(w, r)
	} else {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "404 Not Found: %s\n", r.URL)
	}
}
