package main

import (
	"fmt"
	"log"
	"net/http"
)

// curl http://localhost:8080
//URL.Path= "/"
//curl http://localhost:8080/hello
//Header["User-Agent"] = ["curl/7.79.1"]
//Header["Accept"] = ["*/*"]

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/hello", helloHandler)

	log.Fatalln(http.ListenAndServe(":8080", nil))
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	for k, v := range r.Header {
		fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "URL.Path= %q\n", r.URL.Path)
}
