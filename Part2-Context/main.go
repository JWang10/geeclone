package main

import (
	"gee"
	"net/http"
)

/*
$ curl -i http://localhost:8080
HTTP/1.1 200 OK
Date: Tue, 03 May 2022 17:43:48 GMT
Content-Length: 26
Content-Type: text/plain; charset=utf-8
<h1>Hello, Gee Server</h1>

$ curl "http://localhost:8080/hello?name=test"
hello test, you're at /hello

$ curl "http://localhost:8080/login" -X POST -d "username=test&password=0000"
[{"password":"0000","username":"test"}]
*/

func main() {
	r := gee.New()
	r.Get("/", func(ctx *gee.Context) {
		ctx.HTML(http.StatusOK, "<h1>Hello, Gee Server</h1>")
	})
	r.Get("/hello", func(ctx *gee.Context) {
		// expect /hello?name=[name]
		ctx.String(http.StatusOK, "hello %s, you're at %s\n", ctx.Query("name"), ctx.Path)
	})
	r.Post("/login", func(ctx *gee.Context) {
		ctx.JSON(http.StatusOK, gee.H{
			"username": ctx.PostForm("username"),
			"password": ctx.PostForm("password"),
		})
	})
	r.Run(":8080")
}
