package gee

import (
	"log"
	"net/http"
	"time"
)

func Logger() HandlerFunc {
	return func(ctx *Context) {
		startTimer := time.Now()
		// process request
		ctx.Next()
		// duration from request to response
		log.Printf("[%d] %s - %v", ctx.StatusCode, ctx.Req.RequestURI, time.Since(startTimer))
	}
}

func LoggerOnlyForV2() HandlerFunc {
	return func(ctx *Context) {
		startTimer := time.Now()
		// if the server error occurred
		ctx.Fail(http.StatusInternalServerError, "Internal Server Error")
		log.Printf("[%d] %s - %v for group v2", ctx.StatusCode, ctx.Req.RequestURI, time.Since(startTimer))
	}
}
