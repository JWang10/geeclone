package main

import (
	"gee"
	"net/http"
)

/*
$ curl "http://localhost:8080"
Hello test
$ curl "http://localhost:8080/panic"
[{"message":"Internal Server Error"}]
$ curl "http://localhost:8080"
Hello test

>>> log
2022/05/14 16:26:52 runtime error: index out of range [100] with length 1
Traceback:
        /Users/jwang/sdk/go1.18.1/src/runtime/panic.go:839
        /Users/jwang/sdk/go1.18.1/src/runtime/panic.go:89
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/main.go:34
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/context.go:43
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/recover.go:21
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/context.go:43
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/logger.go:15
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/context.go:43
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/router.go:96
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/gee.go:102
        /Users/jwang/sdk/go1.18.1/src/net/http/server.go:2917
        /Users/jwang/sdk/go1.18.1/src/net/http/server.go:1967
        /Users/jwang/sdk/go1.18.1/src/runtime/asm_arm64.s:1260

2022/05/14 16:26:52 [500] /panic - 265.334µs
2022/05/14 16:26:53 [200] / - 15µs
*/

func main() {
	//r := gee.New()
	//r.Use(gee.Logger()) // global middleware
	//r.Use(gee.Recovery())
	r := gee.Default() // 等於前三行一起運作的情況
	r.Get("/", func(c *gee.Context) {
		c.String(http.StatusOK, "Hello test\n")
	})
	// index out of range for testing Recovery()
	r.Get("/panic", func(c *gee.Context) {
		names := []string{"test"}
		c.String(http.StatusOK, names[100])
	})
	r.Run(":8080")
}
