package gee

import (
	"net/http"
)

// HandlerFunc defines the request handler are uesd by gee
type HandlerFunc func(ctx *Context)

// Engine implements the interface of ServeHTTP (nep/http - Handler interface)
type Engine struct {
	router *router
}

// New a instance of Engine, is the constructor of gee.Engine
func New() *Engine {
	return &Engine{router: newRouter()}
}

func (engine *Engine) addRoute(method, pattern string, handler HandlerFunc) {
	engine.router.addRoute(method, pattern, handler)
}

// Get method request
func (engine *Engine) Get(pattern string, handler HandlerFunc) {
	engine.addRoute("GET", pattern, handler)
}

// Post method request
func (engine *Engine) Post(pattern string, handler HandlerFunc) {
	engine.addRoute("POST", pattern, handler)
}

// Update method request
func (engine *Engine) Update(pattern string, handler HandlerFunc) {
	engine.addRoute("UPDATE", pattern, handler)
}

// Delete method request
func (engine *Engine) Delete(pattern string, handler HandlerFunc) {
	engine.addRoute("DELETE", pattern, handler)
}

// Run defines the method to start a web server,
func (engine *Engine) Run(addr string) (err error) {
	return http.ListenAndServe(addr, engine)
}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c := newContext(w, r)
	engine.router.handle(c)
}
