package main

import (
	"gee"
	"net/http"
)

/*
$ curl "http://localhost:8080/hello/test"
hello test, you're at /hello/test

$ curl "http://localhost:8080/assets/css/test.css"
{"filepath":"css/test.css"}

$ curl "http://localhost:8080/xxx"
404 NOT FOUND: /xxx
*/

func main() {
	r := gee.New()
	r.Get("/", func(ctx *gee.Context) {
		ctx.HTML(http.StatusOK, "<h1>Hello, Gee Server</h1>")
	})
	r.Get("/hello", func(ctx *gee.Context) {
		// expect /hello?name=[name]
		ctx.String(http.StatusOK, "hello %s, you're at %s\n", ctx.Query("name"), ctx.Path)
	})
	r.Get("/hello/:name", func(ctx *gee.Context) {
		// expect /hello/test
		ctx.String(http.StatusOK, "hello %s, you're at %s\n", ctx.Param("name"), ctx.Path)
	})
	r.Post("/login", func(ctx *gee.Context) {
		ctx.JSON(http.StatusOK, gee.H{
			"username": ctx.PostForm("username"),
			"password": ctx.PostForm("password"),
		})
	})
	r.Get("/assets/*filepath", func(ctx *gee.Context) {
		ctx.JSON(http.StatusOK, gee.H{
			"filepath": ctx.Param("filepath"),
		})
	})
	r.Run(":8080")
}
