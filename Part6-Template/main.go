package main

import (
	"fmt"
	"gee"
	"html/template"
	"net/http"
	"time"
)

/*

1. serve static files
$ curl http://localhost:8080/assets/css/test.css
p {
    color: blue;
    font-weight: 700;
    font-size: 20px;
}

2. custom render function
$ curl http://localhost:8080/students
<html>
<body>
    <p>hello, test Template</p>
    <p>0: testA is 18 years old</p>
    <p>1: testB is 20 years old</p>
</body>
</html>

3. render array
$ curl http://localhost:8080/date
<html>
<body>
    <p>hello, gee Test</p>
    <p>Date: 2022-05-14</p>
</body>
</html>
*/

type student struct {
	Name string
	Age  int8
}

func FormatAsDate(t time.Time) string {
	year, month, day := t.Date()
	return fmt.Sprintf("%d-%02d-%02d", year, month, day)
}

func main() {
	r := gee.New()
	r.Use(gee.Logger()) // global middleware
	// Static resource
	r.Static("/assets", "./static")
	r.SetFuncMap(template.FuncMap{
		"FormatAsDate": FormatAsDate,
	})
	r.LoadHTMLGlob("./templates/*")

	student1 := &student{
		Name: "testA",
		Age:  18,
	}
	student2 := &student{
		Name: "testB",
		Age:  20,
	}
	r.Get("/", func(ctx *gee.Context) {
		ctx.HTML(http.StatusOK, "css.tmpl", nil)
	})
	r.Get("/students", func(ctx *gee.Context) {
		ctx.HTML(http.StatusOK, "arr.tmpl", gee.H{
			"title":  "test Template",
			"stuArr": [2]*student{student1, student2},
		})
	})
	r.Get("/date", func(ctx *gee.Context) {
		ctx.HTML(http.StatusOK, "custom_func.tmpl", gee.H{
			"title": "gee Test",
			"now":   time.Date(2022, 05, 14, 0, 0, 0, 0, time.UTC),
		})
	})
	r.Run(":8080")
}
