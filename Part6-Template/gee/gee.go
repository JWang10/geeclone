package gee

import (
	"html/template"
	"log"
	"net/http"
	"path"
	"strings"
)

// HandlerFunc defines the request handler are used by gee
type HandlerFunc func(ctx *Context)

// Engine implements the interface of ServeHTTP (net/http - Handler interface)
type (
	Engine struct { //進一步抽象，讓Engine擁有RouterGroup能力，由Engine統一管理
		*RouterGroup
		router       *router
		groups       []*RouterGroup     //store all groups
		htmlTemplate *template.Template // for html render
		funcMap      template.FuncMap   // for html render
	}
	RouterGroup struct {
		prefix     string
		middleware []HandlerFunc // support middleware
		//parent     *RouterGroup  // support nesting //可以忽略此屬性，因為嵌套在Engine，可以直接透過engine.prefix+prefix取得
		engine *Engine // all groups share an Engine instance
	}
)

// New an instance of Engine, is the constructor of gee.Engine
func New() *Engine {
	engine := &Engine{router: newRouter()}
	engine.RouterGroup = &RouterGroup{engine: engine}
	engine.groups = []*RouterGroup{engine.RouterGroup}
	return engine
}

func (engine *Engine) SetFuncMap(funcMap template.FuncMap) {
	engine.funcMap = funcMap
}
func (engine *Engine) LoadHTMLGlob(pattern string) {
	engine.htmlTemplate = template.Must(template.New("").Funcs(engine.funcMap).ParseGlob(pattern))
}

// Group is defined to create a new RouterGroup
// remember all groups share the same Engine instance
func (group *RouterGroup) Group(prefix string) *RouterGroup {
	engine := group.engine
	newGroup := &RouterGroup{
		prefix: group.prefix + prefix,
		//middleware: nil,
		//parent:     group,
		engine: engine,
	}
	engine.groups = append(engine.groups, newGroup)
	return newGroup
}

func (group *RouterGroup) addRoute(method, pattern string, handler HandlerFunc) {
	newPattern := group.prefix + pattern
	log.Printf("Route %4s - %s\n", method, newPattern)
	group.engine.router.addRoute(method, newPattern, handler)
}

// Get method request
func (group *RouterGroup) Get(pattern string, handler HandlerFunc) {
	group.addRoute("GET", pattern, handler)
}

// Post method request
func (group *RouterGroup) Post(pattern string, handler HandlerFunc) {
	group.addRoute("POST", pattern, handler)
}

// Update method request
func (group *RouterGroup) Update(pattern string, handler HandlerFunc) {
	group.addRoute("UPDATE", pattern, handler)
}

// Delete method request
func (group *RouterGroup) Delete(pattern string, handler HandlerFunc) {
	group.addRoute("DELETE", pattern, handler)
}

// Run defines the method to start a web server,
func (engine *Engine) Run(addr string) (err error) {
	return http.ListenAndServe(addr, engine)
}

// Use is defined to add middleware to the group
func (group *RouterGroup) Use(middlewares ...HandlerFunc) { // 註冊中間件
	group.middleware = append(group.middleware, middlewares...)
}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var middlewares []HandlerFunc
	for _, group := range engine.groups {
		if strings.HasPrefix(r.URL.Path, group.prefix) { // 根據分組路由，判斷是否有分組所使用的中間件
			middlewares = append(middlewares, group.middleware...)
		}
	}
	c := newContext(w, r)
	c.handlers = middlewares // 把中間件交給context對象控制
	c.engine = engine        //因為我們添加新變量engine在Context對象中，使其Context對象能夠通過Engine訪問HTML模板，故實例化時候要賦值
	engine.router.handle(c)
}

// create static handler
func (group *RouterGroup) createStaticHandler(relativePath string, fs http.FileSystem) HandlerFunc {
	absolutePath := path.Join(group.prefix, relativePath)
	fileServer := http.StripPrefix(absolutePath, http.FileServer(fs))
	return func(ctx *Context) {
		filePath := ctx.Param("filepath")

		// Check if file exist and/or if we have permission to access it
		f, err := fs.Open(filePath)
		if err != nil { // if _, err := fs.Open(filePath); err != nil {
			ctx.Status(http.StatusNotFound)
			return
		}
		defer f.Close()

		fileServer.ServeHTTP(ctx.Writer, ctx.Req)
	}
}

// serve static files
func (group *RouterGroup) Static(relativePath string, root string) {
	handler := group.createStaticHandler(relativePath, http.Dir(root))
	urlPattern := path.Join(relativePath, "/*filepath")
	// Register GET handler
	group.Get(urlPattern, handler)
}
