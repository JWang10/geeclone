package main

import (
	"gee"
	"net/http"
)

/*
$ curl "http://localhost:8080/index"
2022/05/10 17:38:41 [200] /index - 6.208µs

$ curl "http://localhost:8080/v2/login" -X POST -d "username=test&password=0000"

2022/05/10 17:39:03 [500] /v2/login - 161.375µs for group v2
2022/05/10 17:39:03 [500] /v2/login - 191.292µs

$ curl "http://localhost:8080/v1/hello"

2022/05/10 17:38:52 [404] /hello - 11.958µs
2022/05/10 17:38:56 [200] /v1/hello - 21.625µs

*/

func main() {
	r := gee.New()
	r.Use(gee.Logger()) // global middleware
	r.Get("/index", func(ctx *gee.Context) {
		ctx.HTML(http.StatusOK, "<h1>Index Page</h1>")
	})

	v1 := r.Group("/v1")
	{
		v1.Get("/", func(ctx *gee.Context) {
			ctx.HTML(http.StatusOK, "<h1>Hello Gee</h1>")
		})
		v1.Get("/hello", func(ctx *gee.Context) {
			// expect /hello?name=test
			ctx.String(http.StatusOK, "hello %s, you're at %s\n", ctx.Query("name"), ctx.Path)
		})
	}

	v2 := r.Group("/v2")
	v2.Use(gee.LoggerOnlyForV2()) // v2 group middleware
	{
		v2.Get("/hello/:name", func(ctx *gee.Context) {
			ctx.String(http.StatusOK, "hello %s, you're at %s\n", ctx.Param("name"), ctx.Path)
		})
		v2.Post("/login", func(ctx *gee.Context) {
			ctx.JSON(http.StatusOK, gee.H{
				"username": ctx.PostForm("username"),
				"password": ctx.PostForm("password"),
			})
		})
	}

	r.Run(":8080")
}
