package gee

import (
	"net/http"
	"strings"
)

type router struct {
	roots    map[string]*node
	handlers map[string]HandlerFunc
}

// roots key,  e.g. roots['GET'] roots['POST']
// handlers key, e.g. handlers['GET-/p/:name/file'], handlers['POST-/p/book']
func newRouter() *router {
	return &router{
		roots:    make(map[string]*node),
		handlers: make(map[string]HandlerFunc)}
}

//Only one "*" is allowed
func parsePattern(pattern string) []string {
	patterns := strings.Split(pattern, "/")

	parts := make([]string, 0)
	for _, item := range patterns {
		if item != "" {
			parts = append(parts, item)
			if item[0] == '*' { // part of pattern is "*", escaping from loop
				break
			}
		}
	}
	return parts
}

func (r *router) addRoute(method string, pattern string, handler HandlerFunc) {
	parts := parsePattern(pattern)

	key := method + "-" + pattern
	_, ok := r.roots[method]
	if !ok {
		r.roots[method] = &node{}
	}
	r.roots[method].insert(pattern, parts, 0)
	r.handlers[key] = handler
}

func (r *router) getRoute(method string, path string) (*node, map[string]string) {
	searchPattern := parsePattern(path)
	params := make(map[string]string) // 子路徑
	root, ok := r.roots[method]
	if !ok {
		return nil, nil
	}

	n := root.search(searchPattern, 0)

	if n != nil {
		parts := parsePattern(n.pattern)
		for idx, part := range parts {
			if part[0] == ':' {
				params[part[1:]] = searchPattern[idx]
			}
			if part[0] == '*' && len(part) > 1 {
				params[part[1:]] = strings.Join(searchPattern[idx:], "/")
			}
		}
		return n, params
	}
	return nil, nil
}

func (r *router) getRoutes(method string) []*node {
	root, ok := r.roots[method]
	if !ok {
		return nil
	}
	nodes := make([]*node, 0)
	root.travel(&nodes) // 遍歷同method的節點路徑
	return nodes
}

func (r *router) handle(c *Context) {
	route, params := r.getRoute(c.Method, c.Path)
	if route != nil {
		c.Params = params
		key := c.Method + "-" + route.pattern
		c.handlers = append(c.handlers, r.handlers[key])
	} else {
		c.handlers = append(c.handlers, func(ctx *Context) {
			c.String(http.StatusNotFound, "404 Not Found: %s\n", c.Path)
		})
	}
	c.Next()
}
