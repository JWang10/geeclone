# GeeClone

a web framework  from scratch refer from geektutu

total parts: 7

##  Part1 - http-base/

### base1

- 基於`net/http`標準庫實現web框架入口
  
  - 調用`http.HandleFunc`實現Route和Handler映射

- http.Handler

  - 實現`http.Handler`接口

從`"net/http"`觀察到，Handler只是一個接口，只要傳入實現Handler接口的`ServeHttp`實例即可
```go
type Handler interface {
ServeHTTP(ResponseWriter, *Request)
}
func ListenAndServe(addr string, handler Handler) error 
```

### base2

在實現了`Engine`實例後，我們攔截所有HTTP請求，持有了唯一入口。
這樣的好處是我們可以`自定義映射規則`，也可以統一加入邏輯處理，如`日誌`、`異常捕獲`等待
```go
// Engine is only a handler for all requests
type Engine struct{}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/":
		fmt.Fprintf(w, "URL.Path= %q\n", r.URL.Path)

	case "/hello":
		for k, v := range r.Header {
			fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
		}
	default:
		fmt.Fprintf(w, "404 Not found: %s\n", r.URL)
	}
}
```

### base 3

定義了`HandlerFunc`類型，提供框架使用者，用來定義路由映射(Route mapping)的處理方法。

我們在`Engine`結構體中，加入了一張路由映射表`router`，Key由請求方法和靜態路由地址構成,
如`[GET]/`、`[GET]/hello`、`[POST]/hello`等，針對不同路由、不同方法，可以映射到不同的
處理方法(Handler)。

當用戶調用`engine.Get`方法時，會將路由和處理方法註冊到router中，
`engine.Run`方法，是`ListenAndServe`的封裝。

## Part2 - Context

> Context設計，提供Json、Html等多種返回類型支援。
> 以Web服務來說，進入點是根據請求`*http.Request`，構造回應`http.ResponseWriter`,
> 但這兩個顆粒度太細了，對於框架用戶來說，需要每次寫重複代碼，如一個完整請求會包含`Header`、
> `Body`，而Header也包含了`StatusCode`，以及`Content-Type`等等。
> 因此將會封裝`Request`和`Response`，簡化輸入操作。

  - Context封裝`Request`和`Response`
    - 也封裝了`HTML`、`String`、`JSON`函數，方便快速建構HTTP回傳值
  - Route獨立出來，增加靈活性
  - Handler參數從`func(Http.ResponseWriter, *Http.Request)`變成`gee.Context`
    - 並提供了`Query`、`PostForm`等參數功能
    
封裝前
```go
obj = map[string]interface{}{
    "name": "geektutu",
    "password": "1234",
}
w.Header().Set("Content-Type", "application/json")
w.WriteHeader(http.StatusOK)
encoder := json.NewEncoder(w)
if err := encoder.Encode(obj); err != nil {
    http.Error(w, err.Error(), 500)
}
```
封裝後
```go
c.JSON(http.StatusOK, gee.H{
    "username": c.PostForm("username"),
    "password": c.PostForm("password"),
})
```

## Part 3 - Route

前面使用了`map`結構儲存了路由資料，索引高效，但只限用靜態路由使用

動態路由，為一條路由規則可以匹配某一類型，而非固定的路由方式
- 如`/hello/:name`，可以匹配`/hello/test`、`hello/world`

- 使用`Trie`樹實現動態路由
![img.png](assets/img.png)

    
- 實現的動態路由具備一下兩種功能
  - 參數匹配
    - 如`/p/:name`
      - 可以匹配`/p/test`和`/p/test2`
  - 通用配置*
    - 如`/assets/*filepath`
      - 可以匹配`/assets/test.css`
      - 可以匹配`/assets/js/jQuery.js`
    - 此種方式常用於靜態伺服器，能夠遞歸匹配子目錄下的路徑

### Context與Handle的變化

在`HandlerFunc`中，如果需要解析`Request`參數，需要對`Context`對象增加一個屬性和方法，
我們將解析後的參數存儲到`Param`中，通過`c.Param([value])`獲取到相對應的值

### 注意事項

- 此處可以多加著墨，有多種路由實現方式，支持的規則、性能等有很大差異
  - 開源路由`gorouter`支持在路由規則中嵌入正則表達式
    - e.g. `/p/[0-9A-Za-z]+`, 意味著路徑僅匹配數字和英文

## Part4 - Group

分組控制是Web框架應提供的基本能力之一。如果沒有路由分組，使得我們需要針對每一個路由進行處理，
但是真實業務場景中，往往某一組路由需要類似的處理。

- `/post`開頭的路由可匿名訪問
- `/admin`開頭的路由需要授權
- `/api`開頭的路由，RESTful接口，對接第三方平台，需要第三方授權
  
大部分情況下的路由分組，都是以相同前綴區分的。

- 此處實現部分也採用`前綴區分`
- 支持嵌套分組
  - 如`/post`是一個分組,`/post/a`、`/post/b`可以是底下的子分組
  - 子分組不僅擁有上層的中間件能力，還可以使用自己專有的中間件

中間件(`middleware`)讓框架有靈活的擴張能力，應用在分組上，可以使分組控制的效益更加明顯，
而不是只共享相同路由前綴。

- `/admin`分組應用授權中間件
- `/`分組應用日誌中間件
  - `/`是默認根節點，表示整個框架都會擁有了日誌紀錄的功能

前面我們採用`(*Engine).addRoute()`來映射所有路由規則和Handler
如果Group對象需要直接映射路由規則的話，如下示意圖調用
```go
r := gee.New()
v1 := r.Group("v1")
v1.Get("/", func(ctx *gee.Context){
	ctx.HTML(http.StatusOK, "<h1>hello Gee Server")
})
```
則`Group`對象，還需要有訪問`Router`的能力，為了方便，我們可以在`Group`中保存一個`指向Engine的指針`，
整個框架資源全由`Engine`統一管理，那麼就可以通過Engine間接訪問各種接口
```go
type Engine struct { //進一步抽象，讓Engine擁有RouterGroup能力，由Engine統一管理
	*RouterGroup 
	router *router
	groups []*RouterGroup // store all groups
}
type RouteGroup struct {
	prefix string
	engine *Engine // all groups share an Engine instance
}

```

### 注意事項

- Go語言的嵌套在其他語言中類似繼承，子類必然是比父類有更多的成員變量和方法
- `RouteGroup`僅負責分組路由功能，`Engine`除了統一管理外，可以有分組路由，還有其他功能
- `RouteGroup`繼承`Engine`的`Run`、`ServeHTTP`方法，軟體設計上是矛盾的
```go
// 此設計也行，依業務設計即可
func New() *RouterGroup {
	engine := &Engine{router: newRouter()}
	return &RouterGroup{
		Engine: engine,
	}
}

func (group *RouterGroup) Group(prefix string) *RouterGroup {
	newGroup := &RouterGroup{
		prefix: group.prefix + prefix,
		Engine: group.Engine,
	}
	group.Engine.groups = append(group.Engine.groups , newGroup)
	return newGroup
}
```

## Part5 - Middleware

Web框架本身不可能去理解所有業務功能，因此需要一個額外的接口，允許用戶自定義功能嵌入框架中，
而這個接口就是此處的`Middleware`(中間件)

- 實現Web框架的中間件機制 (此處參考gin設計方式)
- 實現通用`Logger`中間件
  - 紀錄`Request`到`Response`所花費的時間

對於中間件，需要考慮2個較為關鍵的因素

- 如何插入中間件？
  - 用戶都喜歡直接使用框架功能，如果插入點位於底層，這中間件邏輯將會很複雜
  - 如果插入點離用戶太近，那跟用戶直接定義函數，直接處理`HandlerFunc`沒什麼不同
- 中間件的輸入是什麼？
  - 暴露的參數決定擴展能力

### 中間件設計

中間件定義與前面路由映射的`Handler`相同，處理的輸入為`Context`對象
中間件插入點是框架收到`Request`初始化`Context`對象，允許用戶使用自定義的組件做一些額外處理

  - 日誌紀錄功能
  - 對`Context對象`進行加工
  - 使用`Next`方法，控制中間件順序 (by geektutu)

### 注意事項

- 調用`Next`方式，中間件的調用方式可以討論
- 手工調用`Next`方法， **一般用於請求前後各實現一些行為**
- **如果中間件只作用於請求前，則可以省略調用Next()步驟，為一種兼容性寫法**

```go

func (c *Context) Next() {
	c.index++
	s := len(c.handlers)
	//因不是所有handler都會調用 Next()
	//手工調用 Next()， 一般用於請求前後各實現一些行為
	//如果中間件只作用於請求前，則可以省略調用Next()步驟，為一種兼容性寫法
	for ; c.index < s; c.index++ {
		c.handlers[c.index](c)
	}
}

//func (c *Context) Next() {
//  c.index++
//  c.handlers[c.index](c)
//}
```
- 可參考geektutu對於gin實現中間件方式的解釋
```go
func A(c *Context) {
    part1 // 中間件前半部
    c.Next()
    part2 // 中間件後半部
}
func B(c *Context) {
    part3
    c.Next()
    part4
}
//假设我们应用了中间件 A 和 B，和路由映射的 Handler。c.handlers是这样的[A, B, Handler]，c.index初始化为-1。调用c.Next()，接下来的流程是这样的：
//
//c.index++，c.index 变为 0
//0 < 3，调用 c.handlers[0]，即 A
//执行 part1，调用 c.Next()
//c.index++，c.index 变为 1
//1 < 3，调用 c.handlers[1]，即 B
//执行 part3，调用 c.Next()
//c.index++，c.index 变为 2
//2 < 3，调用 c.handlers[2]，即Handler
//Handler 调用完毕，返回到 B 中的 part4，执行 part4
//part4 执行完毕，返回到 A 中的 part2，执行 part2
//part2 执行完毕，结束。
//一句话说清楚重点，最终的顺序是part1 -> part3 -> Handler -> part 4 -> part2。
```

## Part6 - Template

網頁三元素：`Javascript`、`CSS`、`HTML`
服務端渲染，首先需要支援`CSS`、`HTML`等靜態文件。前面在設計動態路由時候，我們實現了通配符* 
匹配多種子路徑
  - 路由規則
    - `/assets/*filepath`，可以支援匹配`/assets/`開頭的所有路徑

靜態資源服務，所做的就是將路徑的相對位置映射到真實路徑上。
找到文件後，如何返回（回應）這一步，`net/http`包已經實現了。因此我們框架要做的，
只需要`解析請求路徑`，映射到伺服器上的文件真實路徑這一步，交給`http.FileServer`處理即可

此Part實現以下兩點功能：  
- 實現靜態資源服務(Static Resource)
- 支持HTML模板渲染


### 靜態文件 Serve Static Files

用戶訪問`localhost:8080/assets/js/test.js`，會返回`/usr/test/blog/static/js/test.js`
```go
	// Static resource
	r.Static("/assets", "/usr/test/blog/static")
	// 或是相對路徑 r.Static("/assets", "./static")
```

### HTML Template 渲染

Go語言內置了`text/template`和`html/template`2個模板標準庫，其中`html/template`為HTML提供了較為完整的支持，
包括普通變量、列表、對象等渲染。此框架模板渲染直接採用Go內置模板提供的能力

在`Engine`管理上加入Go內置模板`*template.Template`和`template.FuncMap`對象
  - *template.Template
    - 將所有模板加進內存
  - template.FuncMap
    - 自定義模板渲染

並且給用戶提供了`設置自定義渲染函數`和`加載模板方法`

```go
// 自定義渲染函數
func (engine *Engine) SetFuncMap(funcMap template.FuncMap) {
	engine.funcMap = funcMap
}
//加載模板方法
func (engine *Engine) LoadHTMLGlob(pattern string) {
	engine.htmlTemplate = template.Must(template.New("").Funcs(engine.funcMap).ParseGlob(pattern))
}

```

## Part7 - Panic and Recover

Go語言中，比較常見的錯誤處理方法是返回`error`，由調用者決定後續如何處理
但是如果是無法恢復的錯誤，可以手動觸發`panic`

`panic`是Go語言在運行時出現的一個異常情況。
如果異常發生，但沒有被捕捉並恢復，Go程序就會被終止，無論是不是主Goroutine。

雖然此處是說的`panic`異常處理，但跟其他語言提到的`try-catch`，還是有所差異的
一般說的`try-catch`，比較像是有意識到的錯誤，而`panic`則比較像`runtime`中出現的`error`

然而，對於一個web框架，因為某些原因導致系統當機，勢必是不可以接受的，故錯誤處理機制是必要實現的。可能是框架本身沒有完善的測試，導致某些運行下出現空指針異常等情況，
也有可能是來自用戶不正確的參數，觸發異常，如數組超出邊界、空指針等等。

- 前面框架部分沒有錯誤處理機制，如果代碼中有會觸發`panic`的問題，則容易當機
  - 如下示意，數組超出邊界，導致系統crash
```go
func main() {
	r := gee.New()
	r.GET("/panic", func(c *gee.Context) {
		names := []string{"test"}
		c.String(http.StatusOK, names[100])
	})
	r.Run(":9999")
}
```

此最後一部分，會把錯誤處理機制加入框架中
- 以中間件方式，實現錯誤處理機制

```go
$ curl "http://localhost:8080"
Hello test
$ curl "http://localhost:8080/panic"
[{"message":"Internal Server Error"}]
$ curl "http://localhost:8080"
Hello test

>>> log
2022/05/14 16:26:52 runtime error: index out of range [100] with length 1
Traceback:
        /Users/jwang/sdk/go1.18.1/src/runtime/panic.go:839
        /Users/jwang/sdk/go1.18.1/src/runtime/panic.go:89
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/main.go:34
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/context.go:43
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/recover.go:21
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/context.go:43
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/logger.go:15
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/context.go:43
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/router.go:96
        /Users/jwang/Documents/Workspace/GoPractice/Projects/GeeClone/Part7-panic-recover/gee/gee.go:102
        /Users/jwang/sdk/go1.18.1/src/net/http/server.go:2917
        /Users/jwang/sdk/go1.18.1/src/net/http/server.go:1967
        /Users/jwang/sdk/go1.18.1/src/runtime/asm_arm64.s:1260

2022/05/14 16:26:52 [500] /panic - 265.334µs
2022/05/14 16:26:53 [200] / - 15µs
```

### 注意事項

- 搭配`panic`和`recover`，`runtime.Caller`返回調用棧（stack）的計數器
  - 第0個：`Callers`本身
  - 第1個：上一層`trace`
  - 第2個：再上一層的`defer func`
```go
	// Callers用來返回調用stack的的程序計數器
	n := runtime.Callers(3, pcs[:]) // skip first 3 caller
```

- 在`gee`框架裡面，默認整合中間件能力
  - 日誌能力
  - 錯誤處理機制
  - 提供用戶`gee.Default()`或是`gee.New()`使用框架的方式
```go
func Default() *Engine { // 預設中間件，運行日誌及錯誤恢復能力
	engine := New()
	engine.Use(Logger(), Recovery())
	return engine
}
```

