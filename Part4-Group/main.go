package main

import (
	"gee"
	"net/http"
)

/*
$ curl "http://localhost:8080/hello?name=test"
hello test, you're at /hello

$ curl "http://localhost:8080/login" -X POST -d "username=test&password=0000"
[{"password":"0000","username":"test"}]

$ curl "http://localhost:8080/hello/test"
hello test, you're at /hello/test

$ curl "http://localhost:8080/assets/css/test.css"
{"filepath":"css/test.css"}

$ curl "http://localhost:8080/test"
404 NOT FOUND: /test
*/

func main() {
	r := gee.New()

	r.Get("/index", func(ctx *gee.Context) {
		ctx.HTML(http.StatusOK, "<h1>Index Page</h1>")
	})

	v1 := r.Group("/v1")
	{
		v1.Get("/", func(ctx *gee.Context) {
			ctx.HTML(http.StatusOK, "<h1>Hello Gee</h1>")
		})
		v1.Get("/hello", func(ctx *gee.Context) {
			// expect /hello?name=test
			ctx.String(http.StatusOK, "hello %s, you're at %s\n", ctx.Query("name"), ctx.Path)
		})
	}
	v2 := r.Group("/v2")
	{
		v2.Get("/hello/:name", func(ctx *gee.Context) {
			ctx.String(http.StatusOK, "hello %s, you're at %s\n", ctx.Param("name"), ctx.Path)
		})
		v2.Post("/login", func(ctx *gee.Context) {
			ctx.JSON(http.StatusOK, gee.H{
				"username": ctx.PostForm("username"),
				"password": ctx.PostForm("password"),
			})
		})
	}

	r.Run(":8080")
}
